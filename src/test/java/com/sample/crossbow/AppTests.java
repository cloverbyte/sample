package com.sample.crossbow;

import com.sample.crossbow.api.ticket.Ticket;
import com.sample.crossbow.api.ticket.TicketRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@DataJpaTest
public class AppTests {

  @Autowired
  private TicketRepository ticketRepository;

  @Test
  void injectedComponentsAreNotNull() {
    assertThat(ticketRepository).isNotNull();
  }

  @Test
  public void whenInvalidName_thenReturnNull() {
    List<Ticket> fromDb = ticketRepository.findAll();
//    assertThat(fromDb).isNull();
    assertThat(fromDb).isNotNull();
  }

  @Test
  public void whenFindById_thenReturnEmployee() {
    Ticket ticket = new Ticket();
    ticket.setNumber("TEST-2019-0033");
//    testEntityManager.persistAndFlush(ticket);

    ticketRepository.save(ticket);

    Ticket fromDb = ticketRepository.findById(ticket.getId()).orElse(null);
//    assertThat(fromDb.getNumber()).isEqualTo("NOPE");
    assertThat(fromDb.getNumber()).isEqualTo(ticket.getNumber());
  }

}
