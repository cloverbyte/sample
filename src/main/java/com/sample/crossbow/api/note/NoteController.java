package com.sample.crossbow.api.note;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/notes")
public class NoteController {

  @Autowired
  private NoteService noteService;


  @GetMapping
  public ResponseEntity<Iterable<Note>> getNotes() {
    try {
      Iterable<Note> notes = noteService.getNotes();
      return ResponseEntity.ok(notes);
    } catch (Exception ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @GetMapping("/{id}")
  public ResponseEntity<Note> getNote(@PathVariable Long id) {
    try {
      Note note = noteService.getNote(id).orElse(null);
      return ResponseEntity.ok(note);
    } catch (Exception ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @PostMapping
  public ResponseEntity<Note> createNote(@RequestBody Note note) {
    try {
      Note n = noteService.createNote(note);
      return ResponseEntity.ok(n);
    } catch (Exception ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }


}
