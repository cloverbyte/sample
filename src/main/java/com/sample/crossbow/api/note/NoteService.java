package com.sample.crossbow.api.note;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class NoteService {

  @Autowired
  NoteRepository noteRepository;

  Iterable<Note> getNotes() {
    return noteRepository.findAll();
  }

  Optional<Note> getNote(Long id) {
    return noteRepository.findById(id);
  }

  Note createNote(Note note) {
    return noteRepository.save(note);
  }

}
