package com.sample.crossbow.api.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/comments")
public class CommentController {

  @Autowired
  private CommentService commentService;


  @GetMapping
  public ResponseEntity<Iterable<Comment>> getComments() {
    try {
      Iterable<Comment> comments = commentService.getComments();
      return ResponseEntity.ok(comments);
    } catch (Exception ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @GetMapping("/{id}")
  public ResponseEntity<Comment> getComment(@PathVariable Long id) {
    try {
      Comment comment = commentService.getComment(id).orElse(null);
      return ResponseEntity.ok(comment);
    } catch (Exception ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @PostMapping
  public ResponseEntity<Comment> createComment(@RequestBody Comment comment) {
    try {
      Comment c = commentService.createComment(comment);
      return ResponseEntity.ok(c);
    } catch (Exception ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

}
