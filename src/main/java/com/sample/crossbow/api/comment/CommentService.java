package com.sample.crossbow.api.comment;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CommentService {

  @Autowired
  CommentRepository commentRepository;

  Iterable<Comment> getComments() {
    return commentRepository.findAll();
  }

  Optional<Comment> getComment(Long id) {
    return commentRepository.findById(id);
  }

  Comment createComment(Comment comment) {
    return commentRepository.save(comment);
  }

}
