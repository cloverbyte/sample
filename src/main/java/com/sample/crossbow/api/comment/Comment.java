package com.sample.crossbow.api.comment;

import com.sample.crossbow.api.shared.BasicResource;
import com.sample.crossbow.api.ticket.Ticket;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name = "comment")
@Data
//@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Comment extends BasicResource {

  private String text;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn
  private Ticket ticket;

}
