package com.sample.crossbow.api.ticket;

import com.sample.crossbow.api.comment.Comment;
import com.sample.crossbow.api.document.Document;
import com.sample.crossbow.api.note.Note;
import com.sample.crossbow.api.shared.BasicResource;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.List;

@Entity(name = "ticket")
@Data
//@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Ticket extends BasicResource {

  private String number;
  private String type;

  @ManyToMany(cascade = CascadeType.ALL)
  @JoinTable(name = "ticket_document",
      joinColumns = @JoinColumn(name = "document_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "ticket_id",
          referencedColumnName = "id"))
  private List<Document> documents;

  @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Note> notes;

  @OneToMany(mappedBy = "ticket", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Comment> comments;

}
