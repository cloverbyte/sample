package com.sample.crossbow.api.ticket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TicketService {

  @Autowired
  private TicketRepository ticketRepository;

  public List<Ticket> findAll() {
    return ticketRepository.findAll();
  }

  public List<Ticket> findAll(Sort sort) {
    return ticketRepository.findAll(sort);
  }

  public Page<Ticket> findAll(PageRequest pageRequest) {
    return ticketRepository.findAll(pageRequest);
  }

  public Optional<Ticket> findById(Long id) {
    return ticketRepository.findById(id);
  }

  public Ticket create(Ticket ticket) {
    return ticketRepository.save(ticket);
  }

  public Ticket update(Long id, Ticket ticket) {
    Ticket ticketToUpdate = ticketRepository.findById(id).orElse(null);

    //
    if (ticketToUpdate != null) {
//      updateTicket.

      return ticketRepository.save(ticketToUpdate);
    } else {
      return null;
    }
  }

}
