package com.sample.crossbow.api.ticket;

import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/tickets")
@Api(value = "Test Ticket")
public class TicketController {

  @Autowired
  private TicketService ticketService;

  @GetMapping
  public ResponseEntity<Iterable<Ticket>> getTickets(
      @RequestParam(defaultValue = "0") int page,
      @RequestParam(defaultValue = "25") int size,
      @RequestParam(defaultValue = "id") String sortBy,
      @RequestParam(defaultValue = "asc") String direction
  ) {
    try {
      Sort sort;
      PageRequest pageRequest;
      Iterable<Ticket> tickets;

      System.out.println("TESTING");

      if (sortBy != null) {
        // use sorting & pagination

        if (direction != null)
          sort = Sort.by(Sort.Direction.fromString(direction), sortBy);
        else
          sort = Sort.by(sortBy);

        pageRequest = PageRequest.of(page, size, sort);
        tickets = ticketService.findAll(pageRequest);
      } else {
        // use pagination only
        pageRequest = PageRequest.of(page, size);
        tickets = ticketService.findAll(pageRequest);
      }

      return ResponseEntity.ok(tickets);
    } catch (Exception ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @GetMapping("/{id}")
  public ResponseEntity<Ticket> getTicket(@PathVariable Long id) {
    try {
      Ticket ticket = ticketService.findById(id).orElse(null);
      return ResponseEntity.ok(ticket);
    } catch (Exception ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @PostMapping
  public ResponseEntity<Ticket> createTicket(@RequestBody Ticket ticket) {
    try {
      Ticket t = ticketService.create(ticket);
      return ResponseEntity.ok(t);
    } catch (Exception ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @PutMapping("/{id}")
  public ResponseEntity<Ticket> updateTicket(@PathVariable Long id, @RequestBody Ticket ticket) {
    try {
      Ticket t = ticketService.update(id, ticket);
      return ResponseEntity.ok(t);
    } catch (Exception ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }


}
