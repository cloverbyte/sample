package com.sample.crossbow.api.document;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DocumentService {

  @Autowired
  DocumentRepository documentRepository;

  Iterable<Document> getDocuments() {
    return documentRepository.findAll();
  }

  Optional<Document> getDocument(Long id) {
    return documentRepository.findById(id);
  }

  Document createDocument(Document document) {
    return documentRepository.save(document);
  }

}
