package com.sample.crossbow.api.document;

import com.sample.crossbow.api.shared.BasicResource;
import com.sample.crossbow.api.ticket.Ticket;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

@Entity(name = "document")
@Data
//@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class Document extends BasicResource {

  private String text;

  @ManyToMany(mappedBy = "documents")
  private List<Ticket> tickets;

}
