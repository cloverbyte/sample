package com.sample.crossbow.api.document;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/documents")
public class DocumentController {

  @Autowired
  private DocumentService documentService;

  @GetMapping
  public ResponseEntity<Iterable<Document>> getDocuments() {
    try {
      Iterable<Document> documents = documentService.getDocuments();
      return ResponseEntity.ok(documents);
    } catch (Exception ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @GetMapping("/{id}")
  public ResponseEntity<Document> getDocument(@PathVariable Long id) {
    try {
      Document document = documentService.getDocument(id).orElse(null);
      return ResponseEntity.ok(document);
    } catch (Exception ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }

  @PostMapping
  public ResponseEntity<Document> createDocument(@RequestBody Document document) {
    try {
      Document d = documentService.createDocument(document);
      return ResponseEntity.ok(d);
    } catch (Exception ex) {
      return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
  }


}
